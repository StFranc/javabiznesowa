/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabiz.bank.core;

/**
 *
 * @author sfw
 */
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javabiz.bank.core.Account;
import javabiz.bank.core.Bank;
import javabiz.bank.core.Bank.AccountIdException;
import javabiz.bank.core.SimpleBank;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class BankTest {
    private List<Account> accounts;
    private Bank testedBank;
    
    
    @BeforeEach
    void init(){
        this.accounts = new ArrayList<Account>(){{
            add(new Account(1, "Account1", "Address1", 1000));
            add(new Account(2, "Account2", "Address2", 0));
        }};
        this.testedBank = new SimpleBank(this.accounts);
    }
    
    @Test
    void createAccount_DataOfExistingAccount_ReturnsId(){
        Account existingAccount = accounts.get(0);
        
        Integer id = testedBank.createAccount(existingAccount.getName(), existingAccount.getAddress());
        
        assertNotNull(id);
        assertEquals((int)id, existingAccount.getId());
    }
    
    @Test
    void createAccount_DataOfNonExistingAccount_CreatesNewAccountAndReturnsId(){
        int initialAccountsCount = accounts.size();
        String newName = "Account3";
        String newAddress = "Address3";
        
        Integer id = testedBank.createAccount(newName, newAddress);
        List<Account> createdAccount = accounts.stream()
                .filter(a -> a.getId() == (int)id).collect(Collectors.toList());
        
        assertNotNull(id);
        assertTrue((int)id > 0);
        assertEquals(accounts.size(), initialAccountsCount + 1);
        assertEquals(createdAccount.size(), 1);
        assertEquals(createdAccount.get(0).getName(), newName);
        assertEquals(createdAccount.get(0).getAddress(), newAddress);
    }
    
    @Test
    void findAccount_DataOfExistingAccount_ReturnsAccountId(){
        Account existingAccount = accounts.get(0);
        
        Integer id = testedBank.findAccount(existingAccount.getName(), existingAccount.getAddress());
        
        assertNotNull(id);
        assertEquals((int)id, existingAccount.getId());
    }
    
    @Test
    void findAccount_DataOfNonExistingAccount_ReturnsNull(){
        Integer id = testedBank.findAccount("Random Name", "Random Address");
        
        assertNull(id);
    }
    
    @Test
    void deposit_ValidAccountId_DepositsMoneyOnAccount(){
        Account existingAccount = accounts.get(0);
        long initialBalance = existingAccount.getBalane();
        long depositSize = 200;
        
        testedBank.deposit(existingAccount.getId(), depositSize);
        
        assertEquals(existingAccount.getBalane(), initialBalance + depositSize);
    }
    
    @Test
    void deposit_InvalidAccountId_ThrowsAccountIdException(){
        assertThrows(AccountIdException.class, () -> testedBank.deposit(100, 1000));
    }
    
    @Test
    void getBalance_ValidAccountId_ReturnsAccountBalance(){
        Account existingAccount = accounts.get(0);
        long balance = existingAccount.getBalane();
        
        testedBank.getBalance(existingAccount.getId());
        
        assertEquals(existingAccount.getBalane(), balance);
    }
    
    @Test
    void getBalance_InvalidAccountId_ThrowsAccountIdException(){
        assertThrows(Bank.AccountIdException.class, () -> { testedBank.getBalance(123);});
    }
    
    @Test
    void withdraw_ValidAccountIdAndAmount_TransfersFunds(){
        Account existingAccount = accounts.get(0);
        long initialBalance = existingAccount.getBalane();
        long withdrawSize = 200;
        
        testedBank.withdraw(existingAccount.getId(), withdrawSize);
        
        assertEquals(existingAccount.getBalane(), initialBalance - withdrawSize);
    }
    
    @Test
    void withdraw_InvalidAccountId_ThrowsAccountIdException(){
        assertThrows(Bank.AccountIdException.class,() -> {testedBank.withdraw(100, 200);});
    }
    
    @Test
    void withdraw_InvalidAmount_ThrowsInsufficientFundsException(){
        Account existingAccount = accounts.get(0);
        long initialBalance = existingAccount.getBalane();
        long withdrawSize = initialBalance + 1;
        
        assertThrows(Bank.InsufficientFundsException.class,() -> {testedBank.withdraw(existingAccount.getId(), withdrawSize);});
        
        assertEquals(existingAccount.getBalane(), initialBalance);
    }
    
    @Test
    void transfer_ValidAccountIdsAndAmount_TransfersFunds(){
        Account sourceAccount = accounts.get(0);
        Account targetAccount = accounts.get(1);
        
        long initialSourceBalance = sourceAccount.getBalane();
        long initialTargetBalance = targetAccount.getBalane();
        long transferSize = 200;
        
        testedBank.transfer(sourceAccount.getId(), targetAccount.getId(), transferSize);
        
        assertEquals(sourceAccount.getBalane(), initialSourceBalance - transferSize);
        assertEquals(targetAccount.getBalane(), initialTargetBalance + transferSize);
        
    }
    
    @Test
    void transfer_InvalidAccountIds_ThrowsAccountIdException(){
        Account sourceAccount = accounts.get(0);
        Account targetAccount = accounts.get(1);
        
        long initialSourceBalance = sourceAccount.getBalane();
        long initialTargetBalance = targetAccount.getBalane();
        long transferSize = 200;
        
        assertThrows(Bank.AccountIdException.class,() -> { testedBank.transfer(100, targetAccount.getId(), transferSize);});
        assertThrows(Bank.AccountIdException.class,() -> { testedBank.transfer(sourceAccount.getId(), 100, transferSize);;});
        
        assertEquals(sourceAccount.getBalane(), initialSourceBalance);
        assertEquals(targetAccount.getBalane(), initialTargetBalance);
    }
    
    @Test
    void transfer_InvalidAmount_ThrowsInsufficientFundsException(){
        Account sourceAccount = accounts.get(0);
        Account targetAccount = accounts.get(1);
        
        long initialSourceBalance = sourceAccount.getBalane();
        long initialTargetBalance = targetAccount.getBalane();
        long transferSize = initialSourceBalance + 200;
        
        assertThrows(Bank.InsufficientFundsException.class,() -> { testedBank.transfer(targetAccount.getId(), targetAccount.getId(), transferSize);});
        
        assertEquals(sourceAccount.getBalane(), initialSourceBalance);
        assertEquals(targetAccount.getBalane(), initialTargetBalance);
    }
}
