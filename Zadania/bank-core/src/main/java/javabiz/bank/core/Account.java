/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabiz.bank.core;

/**
 *
 * @author sfw
 */
public class Account {
    private int id;
    private String name;
    private String address;
    private long balane;

    public Account(int id, String name, String address, long balane) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.balane = balane;
    }

    public String getAddress() {
        return address;
    }

    public long getBalane() {
        return balane;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setBalane(long balane) {
        this.balane = balane;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
}
