/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabiz.bank.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author sfw
 */

public class SimpleBank implements Bank {
    private List<Account> accounts;

    public SimpleBank(List<Account> accounts) {
        this.accounts = accounts;
    }

    public SimpleBank() {
        accounts = new ArrayList<Account>();
    }
    
    
    
    int getNewId(){
        return accounts.stream()
                .max((a, b) -> a.getId() - b.getId())
                .get().getId() + 1;
    }
    
    @Override
    public Integer createAccount(String name, String address) {
        Optional<Account> account = accounts.stream()
                .filter(a -> a.getAddress().equals(address) && a.getName().equals(name))
                .findAny();
        Integer result = null;
        if(account.isPresent()){
            result = account.get().getId();
        }
        else{
            Account newAccount = new Account(getNewId(), name, address, 0);
            accounts.add(newAccount);
            result = newAccount.getId();
        }
        return result;
    }

    @Override
    public Integer findAccount(String name, String address) {
        Optional<Account> account = accounts.stream()
                .filter(a -> a.getAddress().equals(address) && a.getName().equals(name))
                .findAny();
        Integer result = null;
        if(account.isPresent()) {
            result = account.get().getId();
        }
        else {
            result = null;
        }
        return result;
    }

    @Override
    public void deposit(Integer id, long amount) {
        Optional<Account> account = accounts.stream()
                .filter(a -> a.getId() == id)
                .findAny();
        if(account.isPresent()){
            account.get().setBalane(account.get().getBalane() + amount);
        }
        else{
            throw new Bank.AccountIdException();
        }
    }

    @Override
    public long getBalance(Integer id) {
        Optional<Account> account = accounts.stream()
                .filter(a -> a.getId() == id)
                .findAny();
        if(account.isPresent()){
            return account.get().getBalane();
        }
        else{
            throw new Bank.AccountIdException();
        }
    }

    @Override
    public void withdraw(Integer id, long amount) {
        Optional<Account> account = accounts.stream()
                .filter(a -> a.getId() == id)
                .findAny();
        if(account.isPresent()){
            if(account.get().getBalane() < amount)
                throw new Bank.InsufficientFundsException();
            
            account.get().setBalane(account.get().getBalane() - amount);
        }
        else{
            throw new Bank.AccountIdException();
        }
    }

    @Override
    public void transfer(Integer idSource, Integer idDestination, long amount) {
        Optional<Account> sourceAccount = accounts.stream()
                .filter(a -> a.getId() == idSource)
                .findAny();
        Optional<Account> targetAccount = accounts.stream()
                .filter(a -> a.getId() == idDestination)
                .findAny();
        if(sourceAccount.isPresent() && targetAccount.isPresent()){
            if(sourceAccount.get().getBalane() < amount)
                throw new Bank.InsufficientFundsException();
            
            sourceAccount.get().setBalane(sourceAccount.get().getBalane() - amount);
            targetAccount.get().setBalane(targetAccount.get().getBalane() + amount);
        }
        else {
            throw new Bank.AccountIdException();
        }
    }
    
}
