/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabiz.bank.client;
import org.apache.logging.log4j.Logger;
import java.util.function.Supplier;
import javabiz.bank.core.Bank;

/*
Use Supplier if it takes nothing.

Use Consumer if it returns nothing.

Use Runnable if it does neither.
*/

/**
 *
 * @author sfw
 */
public class BankLogger implements Bank {
    private final Bank bank;
    private final Logger logger;
    
    public BankLogger(Bank bank, Logger logger) {
        this.bank = bank;
        this.logger = logger;
    }
    
    // Metoda-wrapper służąca do obsługiwania wyjątków dla metod z wartością zwrotną
    private <R> R handleExceptions(Supplier<R> operation){
        R result = null;
        try{
            result = operation.get();
        }
        catch(InsufficientFundsException ex){
            logger.error("InsufficientFundsException thrown", ex);
            throw ex;
        }
        catch(AccountIdException ex){
            logger.error("AccountIdException thrown", ex);
            throw ex;
        }
        catch(Throwable t){
            logger.fatal("Unidentified Exception thrown", t);
            throw t;
        }
        return result;
    }
    
    // Metoda-wrapper służąca do obsługiwania wyjątków dla lambd bez wartości zwrotnej
    private void handleExceptions(Runnable operation){
        handleExceptions(() ->
        {
            operation.run();
            return null;
        });
    }

    
    @Override
    public Integer createAccount(String name, String address) {
        return handleExceptions(() -> {
            logger.info("createAccount method called");
            Integer result = bank.createAccount(name, address);
            return result;
        });
    }

    @Override
    public Integer findAccount(String name, String address) {
        return handleExceptions(() -> {
            logger.info("findAccount method called");
            Integer result = bank.findAccount(name, address);
            return result;
        });
    }

    @Override
    public void deposit(Integer id, long amount) {
        handleExceptions(() -> {
            logger.info("deposit method called");
            bank.deposit(id, amount);
        });
    }

    @Override
    public long getBalance(Integer id) {
        return handleExceptions(() -> {
            logger.info("getBalance method called");
            long result = bank.getBalance(id);
            return result;
        });
    }

    @Override
    public void withdraw(Integer id, long amount) {
        handleExceptions(() -> {
            logger.info("withdraw method called");
            bank.withdraw(id, amount);
        });
    }

    @Override
    public void transfer(Integer idSource, Integer idDestination, long amount) {
        handleExceptions(() -> {
            logger.info("transfer method called");
            bank.transfer(idSource, idDestination, amount);
        });
    }
    
}
