/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabiz.bank.client;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javabiz.bank.core.Account;
import javabiz.bank.core.Bank;
import javabiz.bank.core.SimpleBank;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.RollingFileAppender;

/**
 *
 * @author sfw
 */
public class Main {
    public static void main(String[] args) throws URISyntaxException {
        // Tworzymy przykładowe konta
        List<Account> accounts = new ArrayList<Account>() {{
            add(new Account(1, "Account1", "Address1", 1000));
            add(new Account(2, "Account2", "Address2", 500));
        }};
        
        // Tworzymy bank
        Bank bank = new SimpleBank(accounts);
        
        // Tworzymy i konfigurujemy logger
        Logger logger = LogManager.getLogger(Main.class.getName());
        logger.info("Application started");
        
        // Nakładamy na bank aspekt logowania
        Bank bankWithLoggingAspect = new BankLogger(bank, logger);
        
        // Odpalamy CLI banku
        CLI cli = new CLI(bankWithLoggingAspect);
        cli.StartApp();
    }
}
