/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabiz.bank.client;

import java.util.Scanner;
import javabiz.bank.core.Bank;
/**
 *
 * @author sfw
 */
public class CLI {
    private Bank bank;
    private Scanner s;

    public CLI(Bank bank) {
        this.bank = bank;
        this.s = new Scanner(System.in);
    }
    
    public void StartApp(){
        int option = -1;
        while(option != 7){
            displayOptions();
            if(s.hasNextInt() && (option = s.nextInt()) > 0 && option < 8){
                final int tempOption = option;
                handleExceptions(() -> { chooseOperation(tempOption); });
            }
            else{
                s.next();
                System.out.println("Nieprawidłowa opcja, spróbuj ponownie.");
            }
        }
    }
    
    private void displayOptions(){
        System.out.println("1. Stwórz konto,\n2. Znajdź konto,\n3. Zdeponuj,\n4. Pobierz bilans,\n5. Wypłać,\n6. Dokonaj przelewu,\n7. Wyjdź.");
    }
    
    private void chooseOperation(int option){
        switch(option){
            case 1:
                createAccount();
                break;
            case 2:
                findAccount();
                break;
            case 3:
                deposit();
                break;
            case 4:
                getBalance();
                break;
            case 5:
                withdraw();
                break;
            case 6:
                transfer();
                break;
            default:
                break;
        }
    }
    
    private void createAccount(){
        String name;
        String address;
        System.out.println("Podaj nazwę:");
        while(true){
            if(s.hasNext()){
                name = s.next();
                break;
            }
            else{
                s.next();
                System.out.println("Nazwa nieprawidłowa, spróbuj ponownie:");
            }
        }
        System.out.println("Podaj adres:");
        while(true){
            if(s.hasNext()){
                address = s.next();
                break;
            }
            else{
                s.next();
                System.out.println("Adres nieprawidłowy, spróbuj ponownie:");
            }
        }
        System.out.println("Id konta: " + bank.createAccount(name, address));
    }
    
    private void findAccount(){
        String name;
        String address;
        System.out.println("Podaj nazwę:");
        while(true){
            if(s.hasNext()){
                name = s.next();
                break;
            }
            else{
                s.next();
                System.out.println("Nazwa nieprawidłowa, spróbuj ponownie:");
            }
        }
        System.out.println("Podaj adres:");
        while(true){
            if(s.hasNext()){
                address = s.next();
                break;
            }
            else{
                s.next();
                System.out.println("Adres nieprawidłowy, spróbuj ponownie:");
            }
        }
        Integer id = bank.findAccount(name, address);
        if(id != null){
            System.out.println("Id konta: " + id);
        }
        else{
            System.out.println("Szukane konto nie istnieje.");
        }
        
    }
    
    private void deposit(){
        int id;
        long amount;
        System.out.println("Podaj Id konta:");
        while(true){
            if(s.hasNextInt()){
                id = s.nextInt();
                break;
            }
            else{
                s.next();
                System.out.println("Id nieprawidłowe, spróbuj ponownie:");
            }
        }
        System.out.println("Podaj wartość depozytu:");
        while(true){
            if(s.hasNextLong()){
                amount = s.nextLong();
                break;
            }
            else{
                s.next();
                System.out.println("Wartość nieprawidłowa, spróbuj ponownie:");
            }
        }
        
        bank.deposit(id, amount);
        
        System.out.println("Środki zdeponowane pomyślnie.");
    }
    
    private void getBalance(){
        int id;
        System.out.println("Podaj Id konta:");
        while(true){
            if(s.hasNextInt()){
                id = s.nextInt();
                break;
            }
            else{
                s.next();
                System.out.println("Id nieprawidłowe, spróbuj ponownie:");
            }
        }
        System.out.println("Bilans konta: " + bank.getBalance(id));
    }
    
    private void withdraw() {
        int id;
        long amount;
        System.out.println("Podaj Id konta:");
        while(true){
            if(s.hasNextInt()){
                id = s.nextInt();
                break;
            }
            else{
                s.next();
                System.out.println("Id nieprawidłowe, spróbuj ponownie:");
            }
        }
        System.out.println("Podaj wartość wypłaty:");
        while(true){
            if(s.hasNextLong()){
                amount = s.nextLong();
                break;
            }
            else{
                s.next();
                System.out.println("Wartość nieprawidłowa, spróbuj ponownie:");
            }
        }
        
        bank.withdraw(id, amount);
        
        System.out.println("Środki wypłacone pomyślnie.");
    }
    
    private void transfer() {
        int srcId;
        int dstId;
        long amount;
        System.out.println("Podaj Id konta źródłowego:");
        while(true){
            if(s.hasNextInt()){
                srcId = s.nextInt();
                break;
            }
            else{
                s.next();
                System.out.println("Id nieprawidłowe, spróbuj ponownie:");
            }
        }
        System.out.println("Podaj Id konta docelowego:");
        while(true){
            if(s.hasNextInt()){
                dstId= s.nextInt();
                break;
            }
            else{
                s.next();
                System.out.println("Id nieprawidłowe, spróbuj ponownie:");
            }
        }
        System.out.println("Podaj wartość przelewu:");
        while(true){
            if(s.hasNextLong()){
                amount = s.nextLong();
                break;
            }
            else{
                s.next();
                System.out.println("Wartość nieprawidłowa, spróbuj ponownie:");
            }
        }
        
        bank.transfer(srcId, dstId, amount);
        
        System.out.println("Przelew wykonany pomyślnie.");
    }
    
    private void handleExceptions(Runnable operation){
        try{
            operation.run();
        }
        catch(Bank.AccountIdException ex){
            System.out.println("Konto o podanym Id nie istnieje.");
        }
        catch(Bank.InsufficientFundsException ex){
            System.out.println("Niewystarczająca ilość środków na koncie.");
        }
        catch(Throwable t){
            System.out.println("Wystąpił nieprzewidziany problem.");
        }
    }
}
